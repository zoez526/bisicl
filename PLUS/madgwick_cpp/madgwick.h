#include <math.h>
#include <vector>

class Madgwick {
  private:
    static const float gyroMeasError; // gyroscope measurement error in rad/s
    static const float gyroMeasDrift; // gyroscope measurement error in rad/s/s
    static Madgwick* instance;
    float w_bx = 0, w_by = 0, w_bz = 0; // estimate gyroscope biases error
    float b_x, b_z; // reference direction of flux in earth frame;
    float beta;
    float zeta;
    float SEq_1, SEq_2, SEq_3, SEq_4; // estimated orientation quaternion elements with initial conditions
    Madgwick();
  public:
    static Madgwick* getInstance();
    std::vector<float> filterUpdate(std::vector<float> accl, std::vector<float> gyro, std::vector<float> mag, float deltat);
    float getBeta();
    float getZeta();
};
