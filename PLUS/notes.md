# Build Plus On Windows
## Preparation
1. Download and save the clarius sdk at one of the folowing locations
  - C:/program files/clarius\_listen\_plugin/clarius\_listen\_sdk
  - C:/users/usersname/documents/clarius\_listen\_plugin/clarius\_listen\_sdk

2. Download the open source version Qt5.7 from [here](https://www.qt.io/download) and install it at C:\Program Files
	- In the Qt Setup Wizard, select Qt5.7 --> msvc2015_64.
	- After Qt5.7 has been installed, add the binary dirctory of Qt5.7 msvc2015_64 to windows envrionment path by:

3. Download and install Git from [here](https://git-scm.com/download/).

4. Download and install [CMake 3.13.3 or later](https://cmake.org/download)

5. Download and install Visual Studio 2017. In Visual studio intaller, make sure 'VC++ 2017 version 15.9 v14.16 latest v141 tools' is installed.

## Build PLUS
1. In CMake GUI Enable the following dependencies by checking the checkboxes in CMake GUI
  - PLUS\_BUILD\_OPENCV
  - PLUS\_USE\_NDI (enable tracking input through vega)
  - PLUS\_USE\_CLARIUS (enable clarius device)
2. Set the following variables in CMake:
	- PLUSAPP\_GIT\_REPOSITORY = https://gitlab.com/zoez526/PlusApp.git
	- PLUSLIB\_GIT\_REPOSITORY = https://gitlab.com/zoez526/PlusLib.git

## Running PLUS
1. Turn on Clarius Probe. Open the clarius app on phone/tablet
2. Connect Phone/Tablet to the Clarius wifi.
3. Connect computer to the Clarius wifi.
	- wifi name: L7 Probe
	- password: dork!bisicl
4. Double click PlusServerLauncher located at C:/D/PlusB-bin/bin/Release/
5. IMU Streaming:
	- turn on IMU data collection in the clarius app
	- freeze imaging and unfreeze
	- the imu data is saved at C:/D/PlusB-bin/bin/Release/

## Troubleshooting
- LINK Error 1181: cannnot open input file 'vtkGUISupportQt.lib'
  - In CMake GUI, input the following:
    1. Where is the source code: 'C:/D/PlusB-bin/Deps/vtk'
    2. Where to build the binaries: 'C:/D/PlusB-bin/Deps/vtk'
    3. Check both boxes next to 'Grouped' and 'Advanced' Options
    4. Under Module: check the box next to Module\_vtkGUISupportQt
