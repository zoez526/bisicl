import math

def rot_mat_to_euler(rotmat):
    """ Converts a rotation matrix to ZYX Euler angles
    Args:
        rotmat: rotation matrix
    Returns:
        [phy, theta, psi]
        phy: rotation around X
        theta: rotation around Y
        psi: rotation around Z
    """
    if len(rotmat) < 4 or len(rotmat[0]) < 4:
        print("Error converting rotmat to euler")
        print(rotmat);
        return [-1, -1, -1]
    phy = math.atan2(rotmat[2][1], rotmat[2][2])
    theta = -math.atan(rotmat[2][0] / math.pow(math.sqrt(1 - rotmat[2][0]), 2))
    psi = math.atan2(rotmat[1][0], rotmat[0][0])

    euler = [phy, theta, psi]
    return euler
