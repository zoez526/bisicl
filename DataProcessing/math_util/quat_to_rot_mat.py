import numpy

def quat_to_rot_mat(q):
    """ Converts a Quaternion a to rotation matrix
    Args:
        q: quaternion[qr, qi, qj, qk]
    Returns:
        A 4*4 rotational matrix obtained from the input quaternion
    """
    qr = q[0]
    qi = q[1]
    qj = q[2]
    qk = q[3]
    rotmat = numpy.zeros(shape=(4, 4))
    # refer to <https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation> for calculation
    # s = 1 / (magnitude of q)^2;
    s = 1 / (qr * qr +  qi * qi +  qj * qj + qk * qk)
    rotmat[0][0] = 1 - 2 * s * (qj * qj + qk * qk)
    rotmat[0][1] = 2 * s * (qi * qj - qk * qr)
    rotmat[0][2] = 2 * s * (qi * qk + qj * qr)
    rotmat[0][3] = 0
    rotmat[1][0] = 2 * s * (qi * qj + qk * qr)
    rotmat[1][1] = 1 - 2 * s * (qi * qi + qk * qk)
    rotmat[1][2] = 2 * s * (qj * qk - qi * qr)
    rotmat[1][3] = 0
    rotmat[2][0] = 2 * s * (qi * qk - qj * qr)
    rotmat[2][1] = 2 * s * (qj * qk + qi * qr)
    rotmat[2][2] = 1 - 2 * s * (qi * qi + qj * qj)
    rotmat[3] = [0, 0, 0, 1]
    return rotmat
