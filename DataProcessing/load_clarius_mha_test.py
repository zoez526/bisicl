import sys
from data_loaders.mha_metadata import Metadata
"""
Run this module by:
    python3 load_clarius_mha_test.py filename
"""
filename = sys.argv[1]

metadata = Metadata.load_metadata_from_file(filename)

for trk in metadata.tracking_data_list:
    print(trk.__dict__)
    print(metadata.images[trk.FrameNum])
