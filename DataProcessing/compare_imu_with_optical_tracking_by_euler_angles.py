import sys
from data_loaders.load_imu_from_csv import load_imu_from_csv
from data_loaders.load_vega_data_from_mha import load_vega_data_from_mha
from alg.madgwick.madgwick import madgwick
from math_util.rot_mat_to_euler import rot_mat_to_euler
from math_util.quat_to_rot_mat import quat_to_rot_mat
from compare_euler_angles.compare_euler_angles import *
from .match_vega_and_imu_by_tm import match_vega_and_imu_by_tm

"""
Python script for loading and comparing imu to optical tracking
    by converting both to ZYX Euler angles
    and saving the output as a csv file.

Required Arguments:
    1. path to tracked ultrasound(vega + images) saved as mha file.
    2. path to csv file which contains imu data.
    3. file path to save output at.

Run this script by typing:
    python3 compare_imu_with_optical_tracking_by_euler_angles.py vega_tracked_us.mha imu_data.csv output_file.csv
in terminal.
"""

mha_filename = sys.argv[1]
csv_filename = sys.argv[2]

if ('.mha' in mha_filename == False or
        '.csv' in csv_filename == False):
    raise ValueError("Requires first argument to be mha file and second argument to be a csv file")

imu_data = load_imu_from_csv(csv_filename)
vega_data = load_vega_data_from_mha(mha_filename)

# convert raw imu data to quaternions
madgwick = madgwick()
deltat = 0
prevt = 0
for imu in imu_data:
    deltat = deltat if prevt == 0 else float(imu['ImuTimeInSeconds']) - prevt
    prevt = float(imu['ImuTimeInSeconds'])
    madgwick.filter_update(
            float(imu['gx']), float(imu['gy']), float(imu['gz']),
            float(imu['ax']), float(imu['ay']), float(imu['az']),
            float(imu['mx']), float(imu['my']), float(imu['mz']),
            deltat)

# convert imu quaternions to Euler angles
imu_eulers = []
for quat in madgwick.quats:
    imu_eulers.append(rot_mat_to_euler(quat_to_rot_mat(quat)))

# convert vega rotmat to Euler angles
vega_eulers = []
for tracking_data in vega_data:
    # print(tracking_data.ProbeToTrackerTransform)
    vega_eulers.append(rot_mat_to_euler(tracking_data.ProbeToTrackerTransform))

# compare Euler angles by:
# find the nearest times

