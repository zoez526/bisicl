def compare_euler_angles(euler1, euler2):
    """ Calculates the difference between euler angles
    Args:
        euler1: ZYX euler angles [phy1, theta1, psi1]
        euler2: ZYX euler angles [phy2, theta2, psi2]
    Returns:
        [phy1 - phy2, theta1 - theta2, psi1 - psi2]
    Raises:
        ValueError: If length of one the input lists is not 3
        TypeError: If one of the input is not list type
    """
    if (isinstance(euler1, list) == False
            or isinstance(euler2, list) == False):
        raise TypeError("Euler angles should be inputed as list of floats")
    if (len(euler1) != 3
            or len(euler2) != 3):
        raise ValueError("List of euler angles should have length equal to 3")
    result = [0, 0, 0]
    result[0] = euler1[0] - euler2[0]
    result[1] = euler1[1] - euler2[1]
    result[2] = euler1[2] - euler2[2]
    return result

def compare_list_of_euler_angles(list1, list2):
    """Calculates the difference between each entry of the lists
    Args:
        list1: List of ZYX euler angles
        list2: list of ZYX euler angles
    Returns:
        list of difference between each entry of list1 and list2
    """
    if (isinstance(list1, list) == False
            or isinstance(list2, list) == False):
        raise TypeError("Parameters are not of type list")
    if (len(list1) != len(list2)):
        raise ValueError("length of the input params does not match")
    result = []
    for i in range(len(list1)):
        result.append(compare_euler_angles(list1[i], list2[i]))
    return result
