import sys
from data_loaders.load_imu_from_csv import load_imu_from_csv
from data_loaders.mha_metadata import Metadata

"""
Import the match_vega_and_imu_by_tm(imu_filename, mha_filename) function from this module
"""

def match_vega_and_imu_by_tm(imu_filename, mha_filename):
    """
    Args:
        imu_filename: string, path to csv file saved by PLUS
        mha_file: string, path to tracked us file saved as mha file
    Returns:
        List<Pairs<Dict, Dict>>
    """
    # load files
    imu_data = load_imu_from_csv(imu_filename)
    vega_data = Metadata.load_metadata_from_file(mha_filename).tracking_data_list

    # get overlaping frame numbers from tracking data lists and csv file
    imu_min_tm = sys.float_info.max
    imu_max_tm = 0

    for imu in imu_data:
        if imu['ImuConvertedTimestamp'] < imu_min_tm:
            imu_min_tm = imu['ImuConvertedTimestamp']
        if imu['ImuConvertedTimestamp'] > imu_max_tm:
            imu_max_tm = imu ['ImuConvertedTimestamp']

    vega_min_tm = sys.float_info.max
    vega_max_tm = 0
    for vega in vega_data:
        if vega.Timestamp < vega_min_tm:
            vega_min_tm = vega.Timestamp
        if vega.Timestamp > vega_max_tm:
            vega_max_tm = vega.Timestamp

    min_tm = max(vega_min_tm, imu_min_tm)
    max_tm = min(vega_max_tm, imu_max_tm)
    # print("min_tm: ", min_tm)
    # print("max_tm: ", max_tm)

    # cut the lists to only contain the overlapping portion
    imu_data = list(filter(lambda imu: imu['ImuConvertedTimestamp'] >= min_tm and imu['ImuConvertedTimestamp'] <= max_tm, imu_data))
    vega_data = list(filter(lambda vega: vega.Timestamp >= min_tm and vega.Timestamp <= max_tm, vega_data))

    # sort both of them by time
    sorted(imu_data, key=lambda imu: imu['ImuConvertedTimestamp'])
    sorted(vega_data, key=lambda vega: vega.Timestamp)

    # return a list of pairs of imu tracking data and vega tracking data
    imu_ptr = 0
    vega_ptr = 0
    results = []
    while imu_ptr in range(len(imu_data)) and vega_ptr in range(len(vega_data)):
        diff = sys.float_info.max
        pair = {}
        curr_diff = abs(imu_data[imu_ptr]['ImuConvertedTimestamp'] - vega_data[vega_ptr].Timestamp)
        imu_ptr += 1
        while curr_diff < diff and imu_ptr in range(len(imu_data)):
            diff = curr_diff
            curr_diff = abs(imu_data[imu_ptr]['ImuConvertedTimestamp'] - vega_data[vega_ptr].Timestamp)
            pair[0] = imu_data[imu_ptr]
            imu_ptr += 1

        pair[1] = vega_data[vega_ptr].__dict__
        vega_ptr += 1
        # print(pair[0]['ImuConvertedTimestamp'], pair[1].__dict__['Timestamp'])
        results.append(pair)
    return results

