import re
import csv
"""
Loads csv files from disk
To import this module:
    from data_loaders.load_imu_from_csv import load_imu_from_csv

load_imu_from_csv(filename) takes one string argument
    and returns a list of OrderedDict Object.
    eg: [OrderedDict([('FrameNum', 4006.0),
            ('ax', 0.061815), ('ay', -1.303658), ('az', 1.727332),
            ('gx', 5.181909), ('gy', 4.615602), ('gz', -0.783859),
            ('mx', -0.17794), ('my', 1.19266), ('mz', -0.23408),
            ('ImageClariusTimestamp', 468.757767127),
            ('ImagePlusTimestamp', 237.869),
            ('ImageConvertedTimestamp', 237.827683),
            ('ImuClariusTimestamp', 468.840887914),
            ('ImuPlusTimestamp', 237.869),
            ('ImuConvertedTimestamp', 237.91080378700002)])]
"""

def save_key(key):
    """
    Args: column name of the orginial csv file
    Returns: true if key should be accepted
    """
    accepted = ['FrameNum', 'ImuTimeInSeconds',
                'gx', 'gy', 'gz',
                'ax', 'ay', 'az',
                'mx', 'my', 'mz',]
    return key in accepted

def take_timstamp(dict_obj):
    """
    Args:
        an dict object of one row in the original csv file.
    Returns:
        a float, the ImuTimeInSeconds attribute in the dict.
    """
    return float(dict_obj['ImuTimeInSeconds']);

def calculate_imu_system_timestamp(imu_dict):
    """
    Args:
        an dictionary object representing data in one line of the csv file
    Returns:
        add ImuPlusSystemTimestampInSeconds to each imu_dict
    """
    diff = (imu_dict['ImuTimeStamp']- imu_dict['ImageTimestamp']) / pow(10, 9)
    ImusPlusSystemTimestampInSeconds = imu_dict['SystemTimestamp'] + diff
    imu_dict['ImusPlusSystemTimestampInSeconds'] = ImusPlusSystemTimestampInSeconds
    return imu_dict

def calculate_imu_orignial_timstamp(imu_dict):
    ImusClariusTimestamp = imu_dict['ImuTimeStamp'] / pow(10, 9)
    imu_dict['ImuClariusTimestampInSeconds'] = ImusClariusTimestamp
    return imu_dict

def convert_strs_to_floats(row):
    """
    Args:
        OrderedDict object from one line of csv
    Returns:
        boolean indicating if it is a valid csv entry(has numbers as entrys)
    """
    num = re.compile('\-?\d+\.?\d+')
    for pair in row.items():
        if num.match(pair[1]):
            row[pair[0]] = float(pair[1])
        elif pair[0] == '' and pair[1] == '':
            del row[pair[0]]
            continue
        else:
            return False
    return True

def process_row(row):
    """
    Args:
        OrderedDict object representing one row in the csv file
    Returns:
        OrderedDict object with
            - all fields converted to floats
            - all timestamps in seconds
    """
    row['ImageClariusTimestamp'] = row['ImageTimestamp'] / pow(10, 9)
    row['ImagePlusTimestamp'] = row['SystemTimestamp']
    row['ImageConvertedTimestamp'] = row['ConvertedTimestamp']
    row['ImuClariusTimestamp'] = row['ImuTimeStamp'] / pow(10, 9)
    row['ImuPlusTimestamp'] = row['SystemTimestamp']
    row['ImuConvertedTimestamp'] = row['ConvertedTimestamp'] + row['ImuClariusTimestamp'] - row['ImageClariusTimestamp']
    del row['ImageTimestamp']
    del row['ImuTimeStamp']
    del row['ConvertedTimestamp']
    del row['SystemTimestamp']
    return row

def load_imu_from_csv(filename):
    """
    Args:
        csv filename
    Returns:
        [OrderedDict([('FrameNum', 4006.0),
            ('ax', 0.061815), ('ay', -1.303658), ('az', 1.727332),
            ('gx', 5.181909), ('gy', 4.615602), ('gz', -0.783859),
            ('mx', -0.17794), ('my', 1.19266), ('mz', -0.23408),
            ('ImageClariusTimestamp', 468.757767127),
            ('ImagePlusTimestamp', 237.869),
            ('ImageConvertedTimestamp', 237.827683),
            ('ImuClariusTimestamp', 468.840887914),
            ('ImuPlusTimestamp', 237.869),
            ('ImuConvertedTimestamp', 237.91080378700002)])]
    """
    result = []
    with open(filename, 'r') as csvfile:
        imuReader = csv.DictReader(csvfile)
        for row in imuReader:
            if convert_strs_to_floats(row):
                process_row(row)
                result.append(row)
    csvfile.close()
    return result
