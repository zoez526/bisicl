import numpy as np
from mha_metadata import *
from matplotlib import pyplot as plt
gray_img = []

data = Metadata.load_metadata_from_file('../data/20190426-cylinder-fan-recording.mha')

img = data.images
print(img[0])
plt.imshow(img[0], cmap='gray', interpolation='nearest')
plt.show()
