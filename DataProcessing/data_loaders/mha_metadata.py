import numpy as np
import math
import os # operating system module
import re # regex module from functools import reduce
from functools import reduce

"""
Module to read a tracked ultrasound file
Only works with grayscale ultrasound images(1 byte per pixel)
Sample usage:
    # load file:
    metadata = Metadata(filename)
    # get tracking data list:
    metadata.tracking_data_list
    # get individual tracking data:
    metadata.tracking_data_list[frame_num]
    # get images:
    metadata.images
    # get image dimensions
    metadata.images[frame_num].shape
    # get Dimensions:
    metadata.DimSize
"""

class TrackingData:

    def __init__(self):
        self.Timestamp = 0.0 # float
        self.FrameNum = 0 # int
        self.IsTimestampFiltered = False # bool if timestamp is filtered
        self.CommonToTrackerTransform = [] # matrix
        self.CommonToTrackerTransformStatus = False # bool
        self.ProbeToTrackerTransform = [] # matrix
        self.ProbeToTrackerTransformStatus = False # bool
        self.StylusToTrackerTransform = [] # matrix
        self.StylusToTrackerTrasnformStatus = False # bool
        self.ImageStatusOk = False #bool

    @classmethod
    def init_from_frame_num(cls, frame_num):
        trk = TrackingData()
        trk.FrameNum = frame_num
        return trk

    def attr_to_mha_line(self, field):
        line = field + " = "
        value = getattr(self, field);
        if isinstance(value, list):
            for row in value:
                line = line + str(elem)

class Metadata:
    """
    Each Metadata object represents data loaded from one mha file, including tracking and images.
    """
    def __init__(self):
        """
        default constructor
        """
        self.NDims = 3
        self.DimSize = []
        self.ElementType = ''
        self.ElementNumberOfChannels = 4
        self.ElementSpacing = [1, 1, 1]
        self.UltrasoundImageOrientation = ''
        self.UltrasoundImageType = 'B'
        self.header_num_bytes = -1
        self.total_num_bytes = 0
        self.images = []
        self.tracking_data_list = []

    @classmethod
    def load_metadata_from_file(cls, filename):
        """
        Args:
            cls:
                accepts Metadata class as a param rather than a Metadata instance
            filename:
                string, filename of a mha file
        Returns:
            a Metadata instance constructed from the content of the specified mha file
        """
        if not ('.mha' in filename):
            raise ValueError(filename + "is not a mha file")
        metadata = Metadata()
        metadata.read_header(filename)
        metadata.read_images(filename)
        return metadata

    @classmethod
    def set_scalar_attr(cls, instance, param, value):
        setattr(instance, param, int(value))

    @classmethod
    def set_dim_size(cls, instance, param, value):
        instance.set_dim_size(param, value)

    def get_image_shape(self):
        num_channels = 1
        shape = []
        if (self.ElementNumberOfChannels != None):
            num_channels = self.ElementNumberOfChannels
        for i in range(len(self.DimSize) - 1):
            """
            the last element in self.DimSize is the number of frames
            """
            if (self.DimSize[i] != num_channels):
                shape.append(self.DimSize[i])
        shape.append(num_channels)
        return tuple(shape)

    def read_header(self, filename):
        """
        Reads the headers of a mha file. The header portion of a mha file includes
            everything(image timestamps, tracking data, frame numbers)
            except the raw image files.
        Args:
            filename: mha filename
        Returns:
            None
        """
        header_bytes = 0;
        with open(filename, encoding="utf8", errors='ignore') as mha_file:
            flag = True
            while(flag):
                line = mha_file.readline()
                header_bytes += len(line.encode('utf-8'))
                # 'elementdatafile' is last line of header
                if(line.find('ElementDataFile') != -1):
                    flag = False;
                self.read_header_line(line)
        mha_file.close()
        print("header_bytes: ")
        print(header_bytes)
        self.set_total_num_bytes(filename)
        self.header_num_bytes = header_bytes

    def read_images(self, filename):
        """
        Reads row images from file and save images as strings in images
        """
        if (self.header_num_bytes == -1):
            self.set_header_num_bytes()
        mha_file = open(filename, 'rb')
        header = mha_file.read(self.header_num_bytes)
        if (not isinstance(self.DimSize, list)):
            raise TypeError("DimSize is not a list")
        if(len(self.DimSize) == 0):
            raise ValueError("DimSize is empty list")
        num_frames = self.DimSize[-1]
        total_img_size = reduce((lambda x, y: x * y), self.DimSize)
        bytes_per_img = total_img_size / num_frames
        print(num_frames)
        print(total_img_size)
        print(bytes_per_img)
        if math.floor(bytes_per_img) != bytes_per_img:
            raise ValueError('Expecting bytes_per_img to be integer')
        for i in range(num_frames):
            self.save_image(mha_file.read(int(bytes_per_img)))

    def save_image(self, data_bytes):
        """
        Save images as numpy arrays. Array shapes are set by entries in DimSize
        """
        img_shape = tuple(self.DimSize[:-1])
        img = np.frombuffer(data_bytes,dtype=np.uint8)
        img = np.reshape(img, (self.DimSize[1], self.DimSize[0]))
        self.images.append(img)

    def read_header_line(self, line):
        """
        Read headers from mha file
        """
        switcher = {
            'NDims': Metadata.set_scalar_attr,
            'DimSize': Metadata.set_dim_size,
            'ElementType': setattr,
            'UltrasoundImageOrientation': setattr,
            'UltrasoundImageType': setattr,
            'ElementNumberOfChannels': Metadata.set_scalar_attr
            }
        line_pattern = re.compile('(?P<param>[a-zA-Z]*)\s*=\s*(?P<value>([\w\s]*))\n')
        transform_pattern = re.compile('Seq_Frame(?P<frame_num>[0-9]*)_(?P<transform>[a-zA-Z]*)\s*=\s*(?P<matrix>(\d|\s|\.|\-)*)\n')
        status_pattern = re.compile('Seq_Frame(?P<frame_num>[0-9]*)_(?P<transform>[a-zA-Z]*Status)\s*=\s*(?P<status>[a-zA-Z]*)\n')
        # line_pattern matches any headers that does not contain number or underscore
        match = line_pattern.match(line)
        if (match != None):
            param = match.group('param')
            value = match.group('value')
            fn = switcher.get(param, None)
            if (fn != None):
                fn(self, param, value)
            return

        # status_pattern matches any transform status
        # i.e. Seq_Frame0000_ProbeToTrackerTransformStatus
        status_match = status_pattern.match(line)
        if (status_match != None):
            frame_num = int(status_match.group('frame_num'))
            transform_status = status_match.group('transform')
            status = status_match.group('status')
            if (frame_num == len(self.tracking_data_list)):
                self.tracking_data_list.append(
                        TrackingData.init_from_frame_num(frame_num))
            trackingData = self.tracking_data_list[frame_num]
            setattr(trackingData, transform_status, status)
            # print(json.dumps(trackingData. __dict__))
            return

        # transform pattern matches any VALID matrix transforms
        match_transform = transform_pattern.match(line)
        if (match_transform != None):
            frame_num = int(match_transform.group('frame_num'))
            transform = match_transform.group('transform')
            matrix_str = match_transform.group('matrix')
            if (frame_num == len(self.tracking_data_list)):
                self.tracking_data_list.append(
                        TrackingData.init_from_frame_num(frame_num))
            trackingData = self.tracking_data_list[frame_num]
            if (transform == 'Timestamp'):
                setattr(trackingData, transform, float(matrix_str))
                return
            if (transform == 'ImageStatus'):
                setattr(trackingData, transform, matrix_str)
            self.set_transform_by_str(transform, matrix_str, frame_num)
            return

    def str_to_4x4mat(self, matrix_str):
        mat = np.zeros(shape=(4, 4))
        nums = matrix_str.split(' ')
        for i in range(len(mat)):
            for j in range(len(mat[i])):
                mat[i][j] = float(nums[i * 4 + j])
        return mat

    def get_num_bytes_per_pixel(self, element_type):
        """
        Element size reference:
        https://github.com/Kitware/MetaIO/blob/ffe3ce141c5a2394e40a0ecbe2a667cc0566baf5/src/metaTypes.h#L63-94
        """
        element_types = [
                'MET_NONE', 'MET_ASCII_CHAR', 'MET_CHAR', 'MET_UCHAR',
                'MET_SHORT', 'MET_USHORT', 'MET_INT', 'MET_UINT',
                'MET_LONG', 'MET_ULONG', 'MET_LONG_LONG',
                'MET_ULONG_LONG', 'MET_FLOAT', 'MET_DOUBLE',
                'MET_STRING', 'MET_CHAR_ARRAY', 'MET_UCHAR_ARRAY',
                'MET_SHORT_ARRAY', 'MET_USHORT_ARRAY', 'MET_INT_ARRAY',
                'MET_UINT_ARRAY', 'MET_LONG_ARRAY', 'MET_ULONG_ARRAY',
                'MET_LONG_LONG_ARRAY', 'MET_ULONG_LONG_ARRAY',
                'MET_FLOAT_ARRAY', 'MET_DOUBLE_ARRAY',
                'MET_FLOAT_MATRIX', 'MET_OTHER' ]
        sizes = [0, 1, 1, 1, 2, 2, 4, 4, 4, 4, 8, 8, 4, 8,
                 1, 1, 1, 2, 2, 4, 4, 4, 4, 8, 8, 4, 8, 4, 0]
        num_bytes = sizes[element_types.index(element_type)]
        return num_bytes

    def set_total_num_bytes(self, filename):
        self.total_num_bytes = os.path.getsize(filename)
        return self.total_num_bytes

    def set_dim_size(self, param, dimsize_str):
        dims = []
        for num_str in dimsize_str.split(' '):
            if (num_str == ' ' or num_str == ''):
                continue
            dims.append(int(num_str))
        self.DimSize = dims
        print(self.DimSize)
        return self.DimSize

    def set_transform_by_str(self, transform_type, transform_str, frame_num):
        if (len(self.tracking_data_list) == frame_num):
            curr = TrackingData.init_from_frame_num(frame_num)
            self.tracking_data_list.append(curr)
        curr = self.tracking_data_list[frame_num]
        mat = self.str_to_4x4mat(transform_str)
        setattr(curr, transform_type, mat)

    def write_to_file(self, filename):
        if not (os.path.getsize(filename) == 0):
            raise ValueError(filename + " is not empty")
        self.write_headers(filename)
        # self.write_images(filename)

    def write_headers(self, filename):
        with open(filename, 'w+') as mha_file:
            for field in dir(self):
                attr = getattr(self, field)
                if not (type(attr) == 'str' or type(attr) == 'int' or type(attr) != 'list'):
                    print(type(attr))
                    continue
                if (field == 'images' or field == 'tracking_data_list'):
                    continue
                content = str(field) + ' = ' + str(getattr(self, field)) + '\n'
                print(content)
                mha_file.write(content)
        mha_file.close()
        self.write_tracking_data_list(filename)

    def write_tracking_data_list(self, filename):
        """
        prefix = 'Seq_Frame'
        mha_file = open(filename, "a")
        for trk in self.tracking_data_list:
            for field in dir(trk):
                content = trk.attr_to_mha_line(field)
                print(content)
                mha_file.write(content)
        mha_file.write("ElementDataFile = LOCAL")
        """
        return 0

    def convert_image_to_grayscale(self):
        """
        Converts images to grayscale by taking the average or RGB color channels.
        Outputs raw image data as list of numpy 2d array
        """
        imgs = []
        width = 0
        height = 0
        # newFile = open("clarius_images.raw", "w+", encoding='utf-8')
        for idx in range(len(self.images)):
            img = self.images[idx]
            width = img.shape[0]
            height = img.shape[1]
            curr = np.zeros((480, 640))
            for i in range(width):
                for j in range(height):
                    curr[i][j] = int(img[i][j][0])
                    # newFile.write(chr(int(img[i][j][0])))
            # print(img)
            imgs.append(curr)
        # newFile.close()
        return imgs

    def modify_mha_file_for_volume_reconstruction(self, in_filename, out_filename):
        """
        Modifies:
            DimSize(set DimSize to be equal to "width height num_images")
            Images(4 channels to 1 channel)
        Writes output to the out_filename
        """
        if (len(self.images) == 0):
            """
            """
            self = Metadata.load_metadata_from_file(in_filename)
        image_shape = self.get_image_shape()
        num_frames = len(self.images)

