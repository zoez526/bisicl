import sys
from data_loaders.load_imu_from_csv import load_imu_from_csv
"""
Run this module by:
    python3 load_csv_test.py filename
"""

filename = sys.argv[1]

for row in load_imu_from_csv(filename):
    print(row)
    print('\n')
