#ifndef IMFUSION_IMU_DEMO_PLUGIN_H
#define IMFUSION_IMU_DEMO_PLUGIN_H

#include <ImFusion/Base/AlgorithmControllerFactory.h>
#include <ImFusion/Base/AlgorithmFactory.h>
#include <ImFusion/Base/ImFusionPlugin.h>

namespace ImFusion
{
	class IMUDemoAlgorithmFactory : public AlgorithmFactory
	{
	public:
		IMUDemoAlgorithmFactory();
	};

	class IMUDemoControllerFactory : public AlgorithmControllerFactory
	{
	public:
		IMUDemoControllerFactory();
		AlgorithmController* create(Algorithm* a) const override;
	};

	class IMUDemoPlugin : public ImFusionPlugin
	{
	public:
		IMUDemoPlugin();
		virtual ~IMUDemoPlugin();
		virtual const AlgorithmFactory* getAlgorithmFactory();
		virtual const AlgorithmControllerFactory* getAlgorithmControllerFactory();

	private:
		AlgorithmFactory* m_algFactory;
		AlgorithmControllerFactory* m_algCtrlFactory;
	};
}

#endif
