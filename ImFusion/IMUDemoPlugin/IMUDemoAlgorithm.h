/* Copyright (c) 2012-2019 ImFusion GmbH, Munich, Germany. All rights reserved. */
#ifndef IMFUSION_IMU_DEMO_ALGORITHM_H
#define IMFUSION_IMU_DEMO_ALGORITHM_H

#include <ImFusion/Stream/Config.h>
#include <ImFusion/Stream/LiveTrackingStream.h>
#include <ImFusion/Stream/ImageStream.h>
#include <ImFusion/Stream/StreamListener.h>

#include <ImFusion/Base/Algorithm.h>
#include <ImFusion/Base/AlgorithmListener.h>
#include <ImFusion/Base/IMUPoseIntegration.h>
#include <fstream>
namespace ImFusion
{
	class Madgwick;


	class IMUBasedTrackingStream
		: public LiveTrackingStream
		, public StreamListener
	{
	public:
		IMUBasedTrackingStream(const std::string& name);
		~IMUBasedTrackingStream() override;

		/// Return the kind of this data
		Kind kind() const override { return Data::Kind::LIVETRACKINGSTREAM; }

		bool open() override;
		bool close() override;
		bool start() override;
		bool stop() override;
		bool isRunning() const override { return m_running; }

		std::string uuid() override;

		/// Return a copy of tracking instruments
		std::vector<TrackingInstrument*> devices() const override;

		/// Adds an input live stream for processing - will just be used to subscribe for tracking data
		void setInputStream(Stream* stream);

	private:
		// Callback for new data
		void onStreamData(const StreamData& data) override;

		bool writeDataToCsv(const IMURawMetadata* meta, unsigned long long ArrivalTimeStamp);
		// Members
		Stream* m_stream;
		Madgwick* m_madgwick;
		std::unique_ptr<TrackingInstrument> m_ti;
		bool m_running;

		// Write data to csv;
		int frameNum;
		std::ofstream RawImuDataStream;
	};


	class IMUDemoAlgorithm : public Algorithm
	{
	public:
		IMUDemoAlgorithm(Stream* inputStream);

		void compute() override;
		void output(DataList& dataOut) override;

		static bool createCompatible(const DataList& data, Algorithm** a = 0);

		// TODO: Use these to pass parameters
		void configure(const Properties* p) override;
		void configuration(Properties* p) const override;

	private:
		// Members
		Stream* m_inputStream;
		std::unique_ptr<IMUBasedTrackingStream> m_outputStream;
	};
}

#endif
