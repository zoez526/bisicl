#include "IMUDemoPlugin.h"
#include "IMUDemoAlgorithm.h"

#ifdef WIN32
extern "C" __declspec(dllexport) ImFusion::ImFusionPlugin* createPlugin()
{
	return new ImFusion::IMUDemoPlugin;
}
#else
extern "C" ImFusion::ImFusionPlugin* createPlugin()
{
	return new ImFusion::IMUDemoPlugin;
}
#endif

namespace ImFusion
{
	IMUDemoAlgorithmFactory::IMUDemoAlgorithmFactory()
		: AlgorithmFactory("")
	{
		registerAlgorithm<IMUDemoAlgorithm>("IMUDemo;IMU to TrackingStream");
	}

	IMUDemoControllerFactory::IMUDemoControllerFactory() {}

	AlgorithmController* IMUDemoControllerFactory::create(Algorithm* a) const { return nullptr; }

	IMUDemoPlugin::IMUDemoPlugin()
		: m_algFactory(new IMUDemoAlgorithmFactory)
		, m_algCtrlFactory(new IMUDemoControllerFactory)
	{ }

	IMUDemoPlugin::~IMUDemoPlugin() {}

	const ImFusion::AlgorithmFactory* IMUDemoPlugin::getAlgorithmFactory() { return m_algFactory; }

	const ImFusion::AlgorithmControllerFactory* IMUDemoPlugin::getAlgorithmControllerFactory() { return m_algCtrlFactory; }
}
