#include "IMUDemoAlgorithm.h"

#include <ImFusion/Base/DataList.h>
#include <ImFusion/Base/TrackingInstrument.h>
#include <ImFusion/Base/IMUPoseIntegration.h>
#include <ImFusion/Stream/TrackingStreamData.h>
#include <ImFusion/Base/DataComponentFactory.h>

#include <fstream>
#include <string>
namespace ImFusion
{


	IMUBasedTrackingStream::IMUBasedTrackingStream(const std::string& name)
		: LiveTrackingStream(name)
		, m_stream(nullptr)
		, m_madgwick(new Madgwick())
		, m_running(false)
	{
		// TODO: Set useful Madgwick parameters!

		// Template
		m_ti = std::make_unique<TrackingInstrument>();
		m_ti->id = 0;
		m_ti->name = "IMU";
		m_ti->modelNum = "";
		m_ti->matrix = mat4::Identity();
		m_ti->quality = 1;
		m_ti->buttonStatus = 0;
		m_ti->active = true;
		this->frameNum = 0;
		std::ofstream RawImuDataStream;
		RawImuDataStream.open("C:\\Users\\zoez\\Documents\\RawImus_Imf.txt", std::ofstream::app);
		RawImuDataStream << "FrameNum,NumPos,ArrivalTimeStamp,ImuTimeStamp,ax,ay,az,gx,gy,gz,mx,my,mz,\n";
		RawImuDataStream.close();
	}

	IMUBasedTrackingStream::~IMUBasedTrackingStream()
	{
		if(m_stream)
			m_stream->removeListener(this);
		delete m_madgwick;
	}

	void IMUBasedTrackingStream::setInputStream(Stream* stream)
	{
		m_stream = stream;
		stream->addListener(this);
	}

	bool IMUBasedTrackingStream::open()
	{
		m_running = true;
		updateListenersStarted();
		return true;
	}

	bool IMUBasedTrackingStream::close()
	{
		m_running = false;
		updateListenersStopped();
		return true;
	}

	bool IMUBasedTrackingStream::start() { return open(); }

	bool IMUBasedTrackingStream::stop() { return close(); }

	// StreanData& data is the newly received data 
	void IMUBasedTrackingStream::onStreamData(const StreamData& data)
	{
		auto meta = data.components().getSubclass<IMURawMetadata>();
		if (!meta)
			return;

		int npos = meta->m_samples.size();
		
		for (auto i = 0; i < npos; i++) {
			// original timestamp is in nanoseconds, devided by 10 ^ 9
			// acc: g;
			// gyro: dps;
			// mag: gauss;
			m_madgwick->update(meta->m_samples[i].gyro,
							   meta->m_samples[i].linAcc,
							   meta->m_samples[i].mag, 
							   static_cast<double>(meta->m_samples[i].timestamp) * 1e-9);
			std::vector<TrackingInstrument*> tiv;
			auto ti = new TrackingInstrument(*m_ti.get()); // clone
			ti->matrix = mat4::Identity();
			ti->matrix.block<3, 3>(0, 0) = m_madgwick->orientationQuaternion().toRotationMatrix();
			ti->timeStamp = meta->m_samples[i].timestamp * 1e-6;
			tiv.push_back(ti);
			TrackingStreamData tsd(this, tiv);
			tsd.setTimestampArrival(data.timestampArrival());
			tsd.setTimestampDevice(meta->m_samples[i].timestamp * 1e-6);
			Data::setMatrix(ti->matrix);
			updateListenersData(tsd);
		}
		this->writeDataToCsv(meta, data.timestampArrival());
		this->frameNum++;
	}

	bool IMUBasedTrackingStream::writeDataToCsv(const IMURawMetadata* meta, unsigned long long ArrivalTimeStamp) {
		int npos = meta->m_samples.size();
		std::string row = "";
		for (auto i = 0; i < npos; i++) {
			row += (std::to_string(this->frameNum) + ",");
			row += (std::to_string(npos) + ",");
			row += (std::to_string(ArrivalTimeStamp) + ",");
			row += (std::to_string(meta->m_samples[i].timestamp) + ",");
			row += (std::to_string(meta->m_samples[i].linAcc[0]) + ",");
			row += (std::to_string(meta->m_samples[i].linAcc[1]) + ",");
			row += (std::to_string(meta->m_samples[i].linAcc[2]) + ",");
			row += (std::to_string(meta->m_samples[i].gyro[0]) + ",");
			row += (std::to_string(meta->m_samples[i].gyro[1]) + ",");
			row += (std::to_string(meta->m_samples[i].gyro[2]) + ",");
			row += (std::to_string(meta->m_samples[i].mag[0]) + ",");
			row += (std::to_string(meta->m_samples[i].mag[1]) + ",");
			row += (std::to_string(meta->m_samples[i].mag[2]) + ",");
			row += "\n";
		}
		std::ofstream RawImuDataStream;
		RawImuDataStream.open("C:\\Users\\zoez\\Documents\\RawImus_Imf.txt", std::ofstream::app);
		if (RawImuDataStream.is_open() == false) {
			return false;
		}
		RawImuDataStream << row;
		RawImuDataStream.close();
		return true;
	}

	/*
		m_madgwick->update(meta->m_gyro, meta->m_linAcc, meta->m_mag, static_cast<double>(meta->m_timestamp) * 1e-9);

		std::vector<TrackingInstrument*> tiv;
		auto ti = new TrackingInstrument(*m_ti.get()); // clone
		ti->matrix = mat4::Identity();
		ti->matrix.block<3, 3>(0, 0) = m_madgwick->orientationQuaternion().toRotationMatrix();
		ti->timeStamp = data.timestampArrival();
		tiv.push_back(ti);
		TrackingStreamData tsd(this, tiv);
		tsd.setTimestampArrival(data.timestampArrival());
		for (auto i = 0; i < npos; i++) {
			tsd.setTimestampDevice(static_cast<double>(meta->m_samples[i].timestamp) * 1e-6);
		}

		Data::setMatrix(ti->matrix);
		updateListenersData(tsd);
		*/

	std::string IMUBasedTrackingStream::uuid()
	{
		std::stringstream ss;
		ss << this;
		return ss.str();
	}

	std::vector<TrackingInstrument*> IMUBasedTrackingStream::devices() const
	{
		std::vector<TrackingInstrument*> tmp;
		tmp.push_back(new TrackingInstrument(*m_ti.get()));
		return tmp;
	}

	std::unique_ptr<IMURawMetadata> CreateIMURawMetadata() {
		std::unique_ptr<IMURawMetadata> imu(new IMURawMetadata());
		return imu;
	}

	IMUDemoAlgorithm::IMUDemoAlgorithm(Stream* inputStream)
		: m_inputStream(inputStream)
	{
		// Register the IMURawMetadataWith DataComponent factory;
		DataComponentFactory& dataComponentFactory = DataComponentFactory::get();
		IMURawMetadata* imu = new IMURawMetadata();
		dataComponentFactory.registerComponent(imu->id(), CreateIMURawMetadata);
	}
	// Retrieve the properties of this object and stores them in p
	void IMURawMetadata::configuration(Properties* p) const {
		p->setParamType("ax", Properties::ParamType::Double);
		p->setParamType("ay", Properties::ParamType::Double);
		p->setParamType("az", Properties::ParamType::Double);
		p->setParamType("mx", Properties::ParamType::Double);
		p->setParamType("mx", Properties::ParamType::Double);
		p->setParamType("mz", Properties::ParamType::Double);
		p->setParamType("gx", Properties::ParamType::Double);
		p->setParamType("gy", Properties::ParamType::Double);
		p->setParamType("gz", Properties::ParamType::Double);
		p->setParamType("tm", Properties::ParamType::Double);
	}
	// Set one or multiple properties to be the same as p;
	void IMUDemoAlgorithm::configure(const Properties* p) {
		
		return;
	}

	void IMUDemoAlgorithm::configuration(Properties* p) const {
		return;
	}

	/*
	void IMURawMetadata::configure(const Properties* p) {
		return;
	}
	*/

	bool IMUDemoAlgorithm::createCompatible(const DataList& data, Algorithm** a)
	{
		if (data.size() != 1)
			return false;
		auto stream = dynamic_cast<Stream*>(data.getItem(0));
		if (!stream)
			return false;
		if (a)
			*a = new IMUDemoAlgorithm(stream);
		return true;
	}

	void IMUDemoAlgorithm::compute()
	{
		m_outputStream = std::make_unique<IMUBasedTrackingStream>("IMUBasedTrackingStream");
		m_outputStream->setInputStream(m_inputStream);
	}

	void IMUDemoAlgorithm::output(DataList& dataOut)
	{
		if (m_outputStream)
			dataOut.add(m_outputStream.release());
	}
}
