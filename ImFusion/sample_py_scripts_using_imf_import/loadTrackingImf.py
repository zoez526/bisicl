import imfusion; import numpy as np
if not imfusion.app:
    imfusion.app = imfusion.ConsoleController()
# load tracking stream from disk
imfusion.app.open('C:\Temp\ImFusionRecordedSweeps\Patient-19\SampleTrackingStream.ts')
# dm is a collection of all loaded data
dm = imfusion.app.dataModel()
trk = dm.at(0)
# number of frames in the tracking stream
trk.size()
# timestamp of a frame
trk.timestamp(0)
# find all methods implemented by the TrackingStream class
dir(trk)
# get individual tracking frame, which is a 4*4 matrix
trk.rawMatrix(0)
