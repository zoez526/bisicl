import imfusion; import numpy as np
if not imfusion.app:
    imfusion.app = imfusion.ConsoleController()
# load recording from disk
imfusion.app.open('C:\Temp\ImFusionRecordedSweeps\Patient-19\Patient-19-01.imf')
# dm is a collection of all loaded data
dm = imfusion.app.dataModel()
recording = dm.at(0)
img = recording[0]
arr = np.array(img)
# arr.shape should return(480, 640, 4) for Clarius Images;
# 4 indicates number of channels
arr.shape
# access individual pixels
arr[0][0][0]
# 3d matrix to 2d matrix(4 channels to 1)
newImg = np.zeros((480, 640))
for x in range(0, 479):
    for y in range(0, 639):
        newImg[x][y] = np.average(arr[x][y])

