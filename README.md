## Table of content
1. [Overview](#overview)
2. [Current State](#current-state)
3. [Next Steps](#next-steps)
4. [Docs](./Docs/docs.md)

## Overview
The objective for this project is to use [IMU(Inertial measurement unit)](https://en.wikipedia.org/wiki/Inertial_measurement_unit) tracked 2D ultrasound images to reconstruct 3D ultrasound images. 3D ultrasound imaging in the lab was previously done using the Telemed as the ultrasound scaning probe and NDI vega as the optical tracking device. The scaner we are using for this project is the Clarius L7 scanner, which is a portable scanner with has a IMU chip built-in. Obtaining tracking data from the built-in IMU chip holds advantages over using a optical tracker, such as less restriction in the scanning angles, more portability and obtainning tracking and imaging data from the same device.

The following steps are required to achieve the final goal of 3D Volume reconstruction using IMU tracked 2D Clarius Ultrasound images:

- Interface the clarius device sdk with PLUS to enable image streaming from clarius to PLUS.
- Obtain IMU data and convert the IMU to tracking data in the form of homogenous matrix transforms.
- Evaluate the accuracy of IMU tracking data by comparing against optical tracking with NDI vega.
- Reconstruct volumes using clarius images and optical tracking data, as well as clarius images and IMU tracking data. Compare the volumes.

## Current state
### PLUS
[Plus Toolkit](https://plustoolkit.github.io/) is a open source software application for image acquistion, calibration, position tracking, and post-processing such as volume reconstruction. The following functionalities was add to PLUS to allow image and IMU data acquisition from Clarius.

- Implemented and tested c++ code that interfaces the clarius sdk with PLUS by following [this guide](https://plustoolkit.github.io/devicecode) provided by PLUS.
	- Allowed the clarius device to be configured using Plus’s configuration standards.
	- Enabled volume reconstruction, ultrasound video recording of clarius grayscale images.
	- The source code is included in the PlusLib [repository](https://github.com/PlusToolkit/PlusLib/tree/master/src/PlusDataCollection/Clarius).
- Saved IMU data along with reference to PLUS system timestamps and image frame numbers as csv files for further processing.
- Wrote and built C++ implementation of the IMU to quaternion calculation according to [this paper](./Docs/madgwick_internal_report.pdf).
	- This implementation can be included in PLUS to convert IMU data to homogenous matrix transformations in real-time. The advantages of doing this include visualization of tracking stream in 3D slicer and volume reconstruction in 3D slicer using IMU tracking data without post-processing.
	- The C++ code was adapted from the algorithm described [here](./Docs/madgwick_internal_report.pdf).
	- Find the source code and CMake files required for building static and shared libraries [here](./PLUS/madgwick_cpp).

### ImFusion
- Implemented an ImFusion Plugin that enables tracking by IMU data in real-time. To run this plugin in the ImFusion console:
	- Create a Clarius stream (make sure the device is on Freeze, otherwise the Clarius SDK crashes)
	- Launch the IMU demo algorithm, which is a LiveTrackingStream and uses the Magdwick algorithm, by right clicking on the Clarius stream.
Select both the image and the live tracking stream and launch an ultrasound sweep recorder.
	- Alternatively: launch the workspace file (make sure your computer is connected to the clarius wifi and firewall is disabled)
- Configured depth-dependent ultrasound coordinate system calibration during recording.
- Wrote python script for loading ImFusion tracking stream data and images from saved .imf files. The sample python scripts can be found [here](./ImFusion/sample_py_scripts_using_imf_import).
- Provided ImFusion workspace files for ultrasound recording work flows and image coordinate calibrations.
- Relevant ImFusion tickets(you’ll need a ImFusion account to access these):
	- [Depth-dependent us calibration during recording](https://forum.imfusion.com/t/depth-dependent-us-calibration-during-recording/110)
	- [Pixel spacing for clarius images](https://forum.imfusion.com/t/image-spacing-for-ultrasound-images/74/3)
	- [Error loading the clarius plugin](https://forum.imfusion.com/t/clarius-stream-option-missing/109)
	- [Error recording clarius image with tracking stream due to timestamp mismatch](https://forum.imfusion.com/t/error-recording-ultrasound-sweeps-with-clarius-image-and-imu-tracking/52/9)
	- [Calling ImFusion.ConsoleController causes Python console to crush](https://forum.imfusion.com/t/python-console-crushes-when-imfusion-consolecontroller-is-called/124)

### Data processing
- Implemented and tested the python scripts with the following functionalities:
	- Read .mha files from disk and cache image frames, headers and matrix transforms.
	- Load .csv files which contains IMU data from disk and calculate quaternions and Euler angles from IMUs.
	- For a .mha file and .csv file saved from the same sweep, for each frame of optical tracking data, find the IMU data with the closest timestamp.
		- This is required for comparing the accuracy of IMU tracking to optical tracking.

## Functionalities
### Ultrasound image acquisition
Grayscale ultrasound images can be streamed to PLUS and visualized in Slicer via OpenIGTLink in real-time.The images can then be saved in Slicer in the MHA format.

### Optical tracking data acquisition
Optical tracking data are streamed simultaneously with the ultrasound images. This is done by setting both video source and tracking source in the PLUS config file. The transforms that can be saved include ImageToProbe, ImageToTracker, ProbeToTracker [etc](http://perk-software.cs.queensu.ca/plus/doc/nightly/dev/CommonCoordinateSystems.html). Optical tracking and volume reconstruction with NDI Vega was tested.

### IMU acquisition
IMU data are saved in .csv files in while recording ultrasound images. Please note that the Clarius probe only supports IMU data acquisition when scanning in the "Small parts" mode and the IMU data acquisition option is turned on. The IMU data can be loaded and converted to [quaternions](./DataProcessing/alg/madgwick/), [4x4 rotational matrices](./DataProcessing/math_util/quat_to_rot_mat.py), or [euler angles](./DataProcessing/math_util/rot_mat_toeuler.py) in python. The python script for obtaining quaternions from IMU data was adapted from the C code at the end of [this paper](./Docs/madgwick_internal_report.pdf).

### Compare IMU with Optical tracking
Comparing the IMU and Optical tracking data by convert them to ZYX Euler angles and calculate the difference between each entry.

Translational data are <strong>not</strong> calculated from the IMU.

### Volume reconstruction
The volume reconstruction algorithm in PLUS takes in a .mha file contains tracked images and outputs a 3D volume. The configuration options can be found [here](http://perk-software.cs.queensu.ca/plus/doc/nightly/user/AlgorithmVolumeReconstruction.html).

## Next Steps
- Volume reconstruction with IMU tracking:
	- Write clarius images with IMU tracking data as homogenous matrix transforms to .mha files.
	- Run PLUS's volume reconstruction algorithm on the saved .mha file.
 
- Realtime IMU to transformations in PLUS.
	- Add madgwick algorithm and code for converting IMU to quaternions and  quaternions to matrix transforms to PLUS to visualize the IMU data in Slicer in realtime and record the tracking data in mha files.

- Calculate translation using IMU data
	- Currently only rotations are obtained from the IMU data. This only allows volume reconstruction with IMU data using a "rocking" motion (fix the transducer array and swing the scanner back and forth).

## Hardware and software components
- [PLUS](https://plustoolkit.github.io/): an open source software package for tracked ultrasound image acquisition, calibration and processing. The source code is available [here](https://github.com/PlusToolkit).
- [3D Slicer](https://www.slicer.org/): an open source application for medical image informatics, image processing, and three-dimensional visualization.
- [Clarius](https://clarius.com/): The ultrasound probe we use for this project, features built-in [IMU](https://en.wikipedia.org/wiki/Inertial_measurement_unit) chip and wireless data streaming.
- [NDI vega](https://www.ndigital.com/medical/products/polaris-vega/): an optical tracker device. The device API is available [here](https://github.com/PlusToolkit/ndicapi).
- [ImFusion](https://www.imfusion.com/): a software application that is an alternative to the combination of PLUS and 3D Slicer.
