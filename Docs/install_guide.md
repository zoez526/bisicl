# How to install Plus-Clarius on windows10
1. __Qt5.7__
	1. Download Qt5.7 installer [here](https://drive.google.com/drive/folders/1IgG1WPeSU5Llpile-bNkPjHu89v7p4S1?usp=sharing).
	2. Set the installation path to C:/Qt when running the installer.
	3. The Qt folder should look like this: ![](./QtInstallationPath.PNG)

2. __Plus-Clarius__
	1. Dowload Plus-Clarius.zip [here](https://drive.google.com/drive/folders/1IgG1WPeSU5Llpile-bNkPjHu89v7p4S1?usp=sharing).
	2. Find the PlusServerLauncher program in Plus-Clarius.zip.
