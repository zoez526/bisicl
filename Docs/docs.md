## Docs
### Clarius
1. Listen API [Link](https://support.clarius.com/hc/en-us/articles/360020008631-Listen-API) [PDF](../Clarius/ListenAPI_ClariusSupport.pdf)
2. Raw Data Collection [Link](https://support.clarius.com/hc/en-us/articles/360019787932-Raw-Data-Collection) [PDF](../Clarius/RawDataCollection_ClariusSupport.pdf)
3. IMU Spec [PDF](../Clarius/ClariusInertialMeasurementUnitDocs.pdf)
4. 3D Postional Information [Link](https://support.clarius.com/hc/en-us/articles/360024407032-3D-Positional-Information) [PDF](../Clarius/3DPositionalInformation_ClariusSupport.pdf)

### PLUS
1. Compilation instructions
	- [General compilation instruction from PLUS](https://github.com/PlusToolkit/PlusBuild/blob/master/README.md)
	- [Compile the clarius version of PLUS](./install_guide.md)
2. Volume Reconstruction docs
	- Refer to [this page](http://perk-software.cs.queensu.ca/plus/doc/nightly/user/AlgorithmVolumeReconstruction.html) for how to set different input parameters in the config file.The pdf version of this page can be found [here](../PLUS/VolumeReconstructionAlgorithm.pdf).
	- Refer to the [Application](http://perk-software.cs.queensu.ca/plus/doc/nightly/user/ApplicationVolumeReconstructor.html) page for how to run the volume reconstructor from commandline. The pdf version of this page can be found [here](../PLUS/VolumeReconstructorApp.pdf).
3. Coordinate systems
	- Description of the StylusTip, Stylus, Probe, Image coordinate systems can be found [here](http://perk-software.cs.queensu.ca/plus/doc/nightly/dev/CommonCoordinateSystems.html). PDF version available [here](../PLUS/CommonlyUsedCoordinateSystems.pdf).
4. Configuration files
	- PLUS depends on configuration files to determine inputs and outputs. A config file must be provided to run PlusServerLauncher.exe. The default directory of config files is set to C:/D/PlusB-bin/PlusLibData/config. See [this page]() for how to write config files. Sample config files for collecting data using the clarius probe and Vega can be found [here](../PLUS/config). Sampel config files for other devices are [here](https://github.com/PlusToolkit/PlusLibData/tree/master/ConfigFiles).
5. [MHA format guide from ITK](https://itk.org/Wiki/ITK/MetaIO/Documentation)
6. C++ source code that enables PLUS to connect to the Clarius device can be found [here](https://gitlab.com/zoez526/PlusLib/tree/master/src/PlusDataCollection/Clarius).

### ImFusion
1. Coordinate systems
	- [Link to ImFusion forum post on coordinate system convention](https://forum.imfusion.com/t/coordinate-systems-and-transformation-conventions/32)
2. Compile and enable IMUPlugin
	- IMUDemoPlugin Source Code can be found [here](../ImFusion/IMUDemoPlugin).
3. Workspace files
	- ImFusion uses workspace files to save workflows in the ImFusion suite(i.e. which devices are used, which data streams are recorded, which transforms are applied).
	- List of links to workspace files:
        - [Start image and IMU Data streaming](../ImFusion/workspace/Start_Streaming_Image_and_IMU.iws)
        - [Volume Reconstruction using clarius image and imu](../ImFusion/workspace/Record_US_Sweeps_Fixed_Transducer_Array.iws)
4. Relevant tickets(you’ll need a ImFusion account to access these):
	- [Depth-dependent us calibration during recording](https://forum.imfusion.com/t/depth-dependent-us-calibration-during-recording/110)
	- [Pixel spacing for clarius images](https://forum.imfusion.com/t/image-spacing-for-ultrasound-images/74/3)
	- [Error loading the clarius plugin](https://forum.imfusion.com/t/clarius-stream-option-missing/109)
	- [Error recording clarius image with tracking stream due to timestamp mismatch](https://forum.imfusion.com/t/error-recording-ultrasound-sweeps-with-clarius-image-and-imu-tracking/52/9)
	- [Calling ImFusion.ConsoleController causes Python console to crush](https://forum.imfusion.com/t/python-console-crushes-when-imfusion-consolecontroller-is-called/124)

### Python scripts for data processing
1. [Load IMU data from .csv files](../DataProcessing/data_loaders/load_imu_from_csv.py)
	- [Convert IMU to quaternions](../DataProcessing/alg/madgwick)
	- [Convert quaternions to matrix transformations](../DataProcessing/math_util/quat_to_rot_mat.py)
	- [Matrix transformations to Euler angles](../DataProcessing/math_util/rot_mat_to_euler.py)
2. [Load tracked ultrasound data from .mha files](../DataProcessing/data_loaders/mha_metadata.py)
